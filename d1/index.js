// console.log('hello me')

// functions
	//parameters And arguments

	function printInput(){
		let nickName = prompt('enter your nickname: ')
		console.log("hi " +nickName)
	}

	// printInput();


//  however , for the other cases, functions can also process  data directly paassed into it instead of relying on global var and prompt()
	function printName(name){
		console.log("My name is " + name)
	}

	printName("juana");
	
	// you can directly pass data into the function 
	// the function can then call/use that data w.c is referred as "name" w/in the function
	// "name" is called parameter
	// a parameter acts as a named variable/ container that exists only inside a function
	// it is used to store information that is provided to a function when it is called or invoked

	// "juana" the information/data provided derectly into function is called a argument


	// in the ff examples, "john" is arguments since both of them are supplied as inforamtion that will be used to print out the full message
		printName("jonh");

		let sampleVariable= "yui"
		printName(sampleVariable);

	// function argument connot be used by a funcion if there are no parameters provided within the function

	function checkDivisibilityBy8(num){
		let remainder = num % 8;
		console.log("the remainder of " + num + " divided by 8 is: " + remainder);

		let isDivisibleBy8 = remainder === 0;
		console.log("is" +num + "dividible by 8?")
		console.log(isDivisibleBy8)
	}
	checkDivisibilityBy8(64);
	checkDivisibilityBy8(28);

// another
	function checkDivisibilityBy4(num){
		let remainder4 = num % 4;
		console.log("the remainder of " + num + " divided by 4 is: " + remainder4);

		let isDivisibleBy4 = remainder4 === 0;
		console.log("is" + num + "dividible by 4?")
		console.log(isDivisibleBy4)
	}
	checkDivisibilityBy4(56 );
	checkDivisibilityBy4(95);

	// number(prompt())

	// function arguments
	// funtion parameter  can also accept other function as arguments
	// some complex functions use other function as arguments to perform more complicated results
	// this will be further discuss nxt week, array methods


	function argumentFunction(){
		console.log("this function was passed as an argument before the message was printed.")
	};

	function invokeFunction(argumentFunction){
		argumentFunction();
	}

	// adding and removing parentheses'()' impacts the outpus of jS heavily
	// when a function is used w/ parentheses "()", it denotes invoking calling a function

	invokeFunction(argumentFunction);
	// a function used w/out a parenthesis is normally associated w/ usong the function as an argument to another function



	function createdFullName(firstName, middleName, lastName){
		console.log(firstName + '' + middleName +'' +lastName)
	}

	createdFullName('Ralph ', 'Me ', 'yes')

	let firstName = "john";
	let middleName = "doe";
	let lastName = "smith";

	createdFullName(firstName, middleName , lastName);

	function printFullName(middleName, firstName, lastName){
		console.log( firstName + '' + middleName + '' +lastName)
	}
	printFullName('juan' , 'dela' , 'cruz')

	function printFriends(friend1, friend2, friend3){
		console.log("My three friends are " + friend1 + '' + friend2 + '' +friend3)
	}

	printFriends('john ', 'jane ', 'joe')


	// the return statement 
	// the 'return' statement allows us to output a value from a function to be passed to th line/block of code that invoked/called the function

	function returnFullName(firstName, middleName, lastName){
	// console.log(firstName + '' + middleName +'' +lastName);
	return firstName + '' + middleName +'' +lastName;

	console.log("this message will not be printed")
	}

	//  in out example the 'return' statement allows us to output a value of a function to be passed to a line/block of code that called the function
	// the 'returnFullName' function was called in the same line as declaring a variable

	// whatever value was return from "returnFullName" function is store in the "completeName" variable

	let completeName = returnFullName("jeffrey " , 'smith ' , 'bezos')
	console.log(completeName)

	let completeName2 = returnFullName("norgie " , 'V ' , 'bezos')
	console.log(completeName2)

	//  this way a fucntion is able to return a value we can further use or manipulate in our program instead of only printing/displaying it in the console

	console.log(returnFullName(firstName,middleName,lastName))
	// in this example , console log () will print the returned value of the returnFullname function=global variable

	function returnAdress(City , Country){
		let fullAdress= City + ',' + Country;
		return fullAdress 
	};

	let myAddress = returnAdress('cebu city' , 'ph')
	console.log(myAddress)


	function printPlayerInfo(username , level , job){
		// console.log('username'+username)
		// console.log('lvl'+ level)
		// console.log('job'+ job)
		return( "username:" + username + " level: " + level + " job:" + job)
	};

	let user1 =printPlayerInfo( "knight_white" ,96,"paladin")
	console.log(user1)



		function multiple (a,b){
		            return a * b
		        }
		      let  product = multiple(5,5)
		        console.log(product)


